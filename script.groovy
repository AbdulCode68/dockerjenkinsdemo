def buildApp() {
    echo 'application: building phase'
}

def testApp() {
    echo 'application: testing phase'
}

def deployApp() {
    echo 'application: deploying phase'
    echo "deploying version ${params.VERSION}"
}

return this
